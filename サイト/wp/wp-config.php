<?php
/**
 * The base configurations of the WordPress.
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、言語、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - こちらの情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'sddb0040061673');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'sd_dba_ODg0NzQ4');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '3CkSRQPr');

/** MySQL のホスト名 */
define('DB_HOST', 'sddb0040061673.cgidb');

/** データベースのテーブルを作成する際のデータベースのキャラクターセット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z{$y-:n;(zh?lQNC>A+ZLYOCRzte[~= Gu)7*;,ifp}-^px$~^7#c/Dd%6D-.|K/');
define('SECURE_AUTH_KEY',  'z]t5d5t/ps/y6^5emN(V7@(+r0?!4[c4ao&sUy%|+`Mgq.s-}CB0%1><!GRHeG4t');
define('LOGGED_IN_KEY',    'W54w52TB~#Qa]eZ*Q8EwuxXzc&Oe?3D`j+9,?~+{O<<a+S?/wL-*F!i]r+I(n+-Q');
define('NONCE_KEY',        'gV_^LYVpr<yVHwyn7q3@AD7nn},|`HbEoeoqH-2-{/&-3` itXW>L:Gvq}N .vyO');
define('AUTH_SALT',        '~9^o(baP/c~+IXcNWr=[5AS@NC[-c9iH.]b_]e|=QIY3BHOlZ[T*r(3xp#ON2:@z');
define('SECURE_AUTH_SALT', 'Q2GicF5Z9OX[]iuDW[xU`[C6k/C#[3Bs304k[(>!}9,[(Iw:m6|A3CeG6?x#/|6V');
define('LOGGED_IN_SALT',   '|uere]vSkgp5;.v.=+x%[sweT~5n_oALi]i pn5w6={kT_3<@zLq^  Rqu#,j#g=');
define('NONCE_SALT',       'D9#JGk$S{Thnj%Q)^px|*,;{4`T3Tw5f@~4hDeZeN5Z-_0HQu+a1MK+nRU+kKUqS');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * ローカル言語 - このパッケージでは初期値として 'ja' (日本語 UTF-8) が設定されています。
 *
 * WordPress のローカル言語を設定します。設定した言語に対応する MO ファイルが
 * wp-content/languages にインストールされている必要があります。例えば de_DE.mo を
 * wp-content/languages にインストールし WPLANG を 'de_DE' に設定することでドイツ語がサポートされます。
 */
define('WPLANG', 'ja');

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
