<?php
/*
Template Name: コーポレートメッセージ
*/
?>

<?php get_header(); ?>
<body class="page_corporate">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>コーポレートメッセージ</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/corporate/title.png" alt="コーポレートメッセージ" width="672" height="92" /></h3>
    <div class="content_box">
    <h4><img src="<?php bloginfo('template_url')?>/images/corporate/title2.png" alt="世の中の課題に全力で取り組む企業を目指して。" width="511" height="24" /></h4>
    <div><img src="<?php bloginfo('template_url')?>/images/corporate/img.png" alt="スタッフマルシェは人材サービスカンパニーです。
私たちは、人材サービスカンパニーとして、
全ての求職者様に雇用の提供を通じ、
想いに応えるサービスに全力を尽くします。
また、全ての求人者様の様々な課題に、
正面から向き合う人材サービスを全力でご提供致します。
スタッフマルシェは人材サービスを通じ、
世の中の課題解決に全力で取り組み、社会貢献を果たして参ります。" width="654" height="251" /></div>
    </div><!--content_box-->
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>

