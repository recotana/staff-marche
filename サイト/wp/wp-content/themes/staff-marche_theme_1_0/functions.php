<?php
add_editor_style();
function get_post_category_link($post_id) {
  
    //カテゴリーリスト
    $my_cats = get_the_category($post_id);
      
    if ($my_cats) {
      
        $cats_cnt = 0;
        foreach ($my_cats as $my_cat) {
              
            if ($cats_cnt > 0) {
                echo ', ';
            }
  
            $cat_link = get_category_link($my_cat->cat_ID);
  
            echo '<a href="' . $cat_link . '">' . $my_cat->name .'</a>';
              
            $cats_cnt++;
        }
    }
  
}
?>