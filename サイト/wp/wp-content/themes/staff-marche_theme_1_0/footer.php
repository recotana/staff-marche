    </div><!--container-->
    <footer id="footer">
        <ul id="footer_nav" class="clearfix">
            <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
            <li><a href="<?php bloginfo('url')?>/corporate_message/">コーポレートメッセージ</a></li>
            <li><a href="<?php bloginfo('url')?>/aboutus/">会社概要</a></li>
            <li><a href="<?php bloginfo('url')?>/staff/">お仕事をお探しの皆様</a></li>
            <li><a href="<?php bloginfo('url')?>/costmer/">法人のお客様</a></li>
            <li><a href="<?php bloginfo('url')?>/locations/">拠　点</a></li>
            <li><a href="<?php bloginfo('url')?>/contact/">お問い合わせ</a></li>
        </ul>
        <p>Copyright(C) <?php echo date('Y');?> STAFF MARCHE,All Rights Reserved.</p>
    </footer>
</div><!--wrapper-->
<?php wp_footer(); ?>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-24893708-7', 'staff-marche.co.jp');
  ga('send', 'pageview');

</script>
</html>