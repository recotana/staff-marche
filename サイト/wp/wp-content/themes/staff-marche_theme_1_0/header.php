<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
    <title></title>
    <link href="<?php bloginfo('template_url')?>/style.css" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/jquery-1.9.1.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/css_browser_selector.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/html5shiv.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/lib.js"></script>
    <!--[if IE 6]>
    <script src="js/jquery.belatedPNG.min.js"></script>
    <script>DD_belatedPNG.fix('.pngfix');</script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
<?php wp_head(); ?>
</head>
