<?php
/*
Template Name: 短期のお仕事をご希望の皆様へ
*/
?>

<?php get_header(); ?>
<body class="page_require">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>短期のお仕事をご希望の皆様へ</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/requirement/title.png" alt="title" width="342" height="22"></h3>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content();?>
<?php endwhile; ?>
<?php else : ?>
現在、記事はありません
<?php endif; ?>
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>
