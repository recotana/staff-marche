<?php get_header(); ?>
<body class="post_single">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/single/title.png" alt="title" width="672" height="92"></h3>
    <div class="content_box">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<div class="section">
<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
<?php the_content(); ?>
<div class="post_data"><?php the_time('Y/m/d'); ?>　カテゴリー : <?php get_post_category_link($post_id); ?> </div>
<div class="social">
<?php 
    if(function_exists("wp_social_bookmarking_light_output_e")){
        wp_social_bookmarking_light_output_e();
    }?>
</div><!-- social –>
</div><!--section-->
<?php endwhile; ?>
<?php else : ?>
現在、記事はありません
<?php endif; ?>
</div><!--content_box-->
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>