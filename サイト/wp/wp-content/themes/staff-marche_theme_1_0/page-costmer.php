<?php
/*
Template Name: 法人のお客様
*/
?>
<?php get_header(); ?>
<body class="page_costmer">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>法人のお客様</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
    <h3><img src="<?php bloginfo('template_url')?>/images/costmer/title.png" alt="法人のお客様" width="672" height="92" /></h3>
    <div class="description">
        <p><?php echo get_field('comment'); ?></p>
    </div>
    <div class="flow">
        <h3 class="lead_flow"><img src="<?php bloginfo('template_url')?>/images/costmer/flow_service.png" alt="flow_service" width="656" height="34"></h3>
        <img class="consul_img" src="<?php bloginfo('template_url')?>/images/costmer/flow_img.png" alt="flow_img" width="656" height="132">
        <ul class="service">
            <li><img src="<?php bloginfo('template_url')?>/images/costmer/service1.png" alt="service1" width="475" height="69"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/costmer/service2.png" alt="service1" width="475" height="69"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/costmer/service3.png" alt="service1" width="475" height="69"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/costmer/service4.png" alt="service1" width="475" height="69"></li>
        </ul>
        <h4 class="lead_contact2"><img src="<?php bloginfo('template_url')?>/images/costmer/lead_contact.png" alt="お問い合わせ先" width="176" height="24" /></h4>
        <ul class="consul">
            <li><img src="<?php bloginfo('template_url')?>/images/costmer/lead_tel.png" alt="lead_tel" width="96" height="24"><img src="<?php bloginfo('template_url')?>/images/costmer/tel.png" alt="tel" width="392" height="24"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/costmer/lead_mail.png" alt="lead_mail" width="96" height="24"><a href="mailto:info@staff-marche.co.jp"><img src="<?php bloginfo('template_url')?>/images/costmer/mail.png" alt="mail" width="263" height="24"></a></li>
        </ul><!--consul-->
        <img class="attention_form" src="<?php bloginfo('template_url')?>/images/common/form/attention_form.png" alt="attention_form" width="556" height="24">
        <div class="form_box">
           <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
            <?php endwhile; ?>
            <?php else : ?>
            現在、記事はありません
            <?php endif; ?>

        </div>
    </div>
    
</div><!--content-->


</div><!--content_body-->
<?php get_footer(); ?>
