<div id="head_line"></div>
<div id="container">

<header id="header" class="clearfix">
    <h1 id="header_logo"><a href="<?php bloginfo('home');?>/"><span></span></a></h1>
    <ul id="head_nav" class="clearfix">
        <li class="hnav_row2 txt_size_11"><a href="<?php bloginfo('url')?>/staff/">お仕事を<br/>お探しの皆様</a></li>
        <li class="hnav_row2 txt_size_11"><a href="<?php bloginfo('url')?>/costmer/">法人の<br/>お客様</a></li>
        <li class="hnav_row2 txt_size_11"><a href="<?php bloginfo('url')?>/corporate_message/">コーポレート<br/>メッセージ</a></li>
        <li><a href="<?php bloginfo('url')?>/aboutus/">会社概要</a></li>
        <li><a href="<?php bloginfo('url')?>/locations/">拠　点</a></li>
        <li class="txt_size_11"><a href="<?php bloginfo('url')?>/contact/">お問い合わせ</a></li>
    </ul>
</header>