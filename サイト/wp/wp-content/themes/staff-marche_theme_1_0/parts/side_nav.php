<div id="side_nav">
    <ul>
        <li id="snav_staff"><a href="<?php bloginfo('url')?>/staff/"><span>お仕事をお探しの皆様</span></a></li>
        <li id="snav_costmer"><a href="<?php bloginfo('url')?>/costmer/"><span>法人のお客様<</span></a></li>
        <li id="snav_corporate"><a href="<?php bloginfo('url')?>/corporate_message/"><span>コーポレートメッセージ</span></a></li>
        <li id="snav_aboutus"><a href="<?php bloginfo('url')?>/aboutus/"><span>会社概要</span></a></li>
        <li id="snav_shops"><a href="<?php bloginfo('url')?>/locations/"><span>拠点</span></a></li>
        <li id="snav_contact"><a href="<?php bloginfo('url')?>/contact/"><span>お問い合わせ</span></a></li>
        <li id="snav_recruit"><a href="<?php bloginfo('url')?>/recruit/"><span>採用情報</span></a></li>
    </ul>
</div><!--side_nav-->