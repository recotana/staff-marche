<div id="side_nav">
    <ul>
        <li id="snav_staff"><a href="<?php bloginfo('url')?>/staff/"><span>お仕事をお探しの皆様</span></a></li>
        <li id="snav_costmer"><a href="<?php bloginfo('url')?>/costmer/"><span>法人のお客様<</span></a></li>
        <li id="snav_aboutus"><a href="<?php bloginfo('url')?>/aboutus/"><span>会社概要</span></a></li>
        <li id="snav_shops"><a href="<?php bloginfo('url')?>/shops/"><span>拠点</span></a></li>
        <li id="snav_contact"><a href="<?php bloginfo('url')?>/contact/"><span>お問い合わせ</span></a></li>
        <li id="snav_recruit"><a href="<?php bloginfo('url')?>/recruit/"><span>採用情報</span></a></li>
    </ul>
    <div id="category_list">
        <h4>カテゴリ一</h4>
        <ul>
        <?php wp_list_categories('title_li=&depth=1&show_count=1'); ?>
        </ul>
    </div>
</div><!--side_nav-->