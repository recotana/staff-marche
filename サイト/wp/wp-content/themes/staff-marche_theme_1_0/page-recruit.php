<?php
/*
Template Name: 採用情報
*/
?>

<?php get_header(); ?>
<body class="page_recruit">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>採用情報</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/recruit/title.png" alt="title" width="202" height="23" /></h3>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<div class="content_box">
<?php the_content();?>
</div>
<?php endwhile; ?>
<?php else : ?>
現在、記事はありません
<?php endif; ?>
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>

