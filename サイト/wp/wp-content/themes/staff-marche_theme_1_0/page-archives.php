<?php
/*
Template Name: 記事アーカイブ
*/
?>
<?php get_header(); ?>
<body class="post_archive">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>募集内容一覧</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_category') ?>

<div id="content">
    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/info/title.png" alt="title" width="672" height="92"></h3>
    <div class="content_box">
<?php query_posts('post_type=post&paged='.$paged); ?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<div class="section dash_border">
<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
<?php the_content(); ?>
<div class="post_data"><?php the_time('Y/m/d'); ?>　カテゴリー : <?php get_post_category_link($post_id); ?> </div>

</div><!--section-->
<?php endwhile; endif; ?>
<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
<?php wp_reset_query(); ?>

</div><!--content_box-->
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>

