<?php get_header(); ?>
<body class="page_home">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
        
<div id="content_body">
    <div  id="main_visual">
    <ul>
        <li class="mv_3">
            <img src="<?php bloginfo('template_url')?>/images/top/mv3.png" alt="mv1" width="880" height="240">
            <a href="<?php bloginfo('url')?>/costmer/"></a>
        </li>
        <li class="mv_2">
            <img src="<?php bloginfo('template_url')?>/images/top/mv2.png" alt="mv1" width="880" height="240">
            <a href="<?php bloginfo('url')?>/staff/"></a>
        </li>
        <li class="mv_1">
            <img src="<?php bloginfo('template_url')?>/images/top/mv1.png" alt="mv1" width="880" height="240">
            <a href="<?php bloginfo('url')?>/aboutus/"></a>
        </li>
        
    </ul>
    </div>
    
    <div id="mv_thumb" class="clearfix">
        <ul>
            <li class="thumb_1"><img src="<?php bloginfo('template_url')?>/images/top/thumb1.png" alt="thumb1" width="110" height="30"></li>
            <li class="thumb_2"><img src="<?php bloginfo('template_url')?>/images/top/thumb2.png" alt="thumb2" width="110" height="30"></li>
            <li class="thumb_3"><img src="<?php bloginfo('template_url')?>/images/top/thumb3.png" alt="thumb3" width="110" height="30"></li>
        </ul>
    </div><!-mv_thumb-->
    
    <ul id="main_nav" class="clearfix">
        <li id="mnav_staff"><a href="<?php bloginfo('url')?>/staff/"><span>お仕事をお探しの皆様</span></a></li>
        <li id="mnav_costmer"><a href="<?php bloginfo('url')?>/costmer/"><span>法人のお客様</span></a></li>
        <li id="mnav_corporate"><a href="<?php bloginfo('url')?>/corporate_message/"><span>コーポレートメッセージ</span></a></li>
        <li id="mnav_aboutus"><a href="<?php bloginfo('url')?>/aboutus/"><span>会社概要</span></a></li>
        <li id="mnav_shops"><a href="<?php bloginfo('url')?>/locations/"><span>拠点</span></a></li>
        <li id="mnav_contact"><a href="<?php bloginfo('url')?>/contact/"><span>お問い合わせ</span></a></li>
    </ul>
    <div id="info_section" class="clearfix">
        <div id="info_outer">
        <div id="info_box">
            <h2>新着情報</h2>
            <ul>
                <?php query_posts('post_type=post&showposts=4'); ?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<li class="info_list">
<dl class="clearfix">
<dt><?php the_time('Y.n.j');?></dt>
<dd><?php the_title();?></dd>
<a href="<?php the_permalink();?>" ></a>
</dl>
</li>
<?php endwhile; endif; ?>
            </ul>
            <a href="<?php bloginfo('url')?>/archive/" class="bt_infoarchive">一覧はこちら</a>
        </div><!--info_box-->
        </div><!--info_outer-->
        
        <div id="button_box">
            <a href="<?php bloginfo('url')?>/recruit/"><img src="<?php bloginfo('template_url')?>/images/top/bt_saiyou.png" alt="採用情報" width="345" height="60" /></a>
            <a href="<?php bloginfo('url')?>/privacy/"><img src="<?php bloginfo('template_url')?>/images/top/bt_kojin.png" alt="個人情報のお取扱いについて" width="345" height="60" /></a>
            <a href="<?php bloginfo('url')?>/requirement/"><img src="<?php bloginfo('template_url')?>/images/top/bt_tanki.png" alt="短期のお仕事をご希望の皆様へ" width="345" height="60" /></a>
        </div><!--button_box-->
    </div><!--info_section-->
    
</div><!--content_body-->
<?php get_footer(); ?>