var current=3;
var next=1;
var timer;
$(function(){
    $('.info_list').click(function(){
        window.location = $(this).find('a').attr('href'); 
    });
    
    anim(0);
    timer = setInterval("anim()",8000);
    $('#main_visual ul li').click(function(){
        window.location = $(this).find('a').attr('href'); 
    });
    
    $('#mv_thumb ul li').click(function(){
        if($(this).hasClass('thumb_1')) next=1;
        else if($(this).hasClass('thumb_2')) next=2;
        else if($(this).hasClass('thumb_3')) next=3;
        clearInterval(timer);
        anim(1);
        timer = setInterval("anim(0)",8000);
    });
    
    
   
    privacyCheck();
    
    $('#privacy_check').click(function(){
        privacyCheck();
    });
    
    if( $('.form_box div').hasClass('mw_wp_form_preview') ){
        $('body').addClass('form_parts_hidden');
        $('body').addClass('form_input_comfirm');
    }
    else{
        $('body').removeClass('form_parts_hidden');
    }
    
    if( $('.form_box p').hasClass('form_success') ){
        $('body').addClass('form_parts_hidden');
        $('body').addClass('form_send_complete');
    }

    
    $('#side_nav li a span,#main_nav li a span,#header_logo a span').hover(
        function(){
            $(this).stop().animate({'opacity':0},{duration:300});
        },
        function(){
            $(this).stop().animate({'opacity':1},{duration:100});
        }
    );

});

function anim(force){

    if(force!=1){
        next=current+1;
        if(next>3) next=1;
    }
    
    var nextClass='.mv_'+next;
    var oldClass='.mv_'+current;
    var nextThumbClass='.thumb_'+next;
    var oldThumbClass='.thumb_'+current;
    
    $(oldClass).css('z-index',50);
    $(oldThumbClass).css('border-color','#aaa');
    $(nextThumbClass).css('border-color','#c0000a');
    $(nextClass).css('z-index',100).animate({'opacity':1},{duration:1000,complete:function(){
        $(oldClass).css('opacity',0).css('z-index',1);
        current=next;
    }});
}

function privacyCheck(){
    if($('#privacy_check').prop('checked') == false){
             $('input[name="submitPreview"]').attr("disabled","disabled");
    }
    else {
            $('input[name="submitPreview"]').removeAttr('disabled');
    }
}