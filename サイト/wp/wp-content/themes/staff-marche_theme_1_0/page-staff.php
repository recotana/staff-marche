<?php
/*
Template Name: お仕事をお探しの皆様
*/
?>
<?php get_header(); ?>
<body class="page_staff">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>お仕事をお探しの皆様</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">

    
    <?php get_template_part('parts/side_nav') ?>
    
    <div id="content">
        <h3 ><img src="<?php bloginfo('template_url')?>/images/forstaff/title.png" alt="お仕事をお探しの皆様" width="672" height="92" /></h3>
        <div class="description">
            <p><?php echo get_field('comment'); ?></p>
        </div>
        <div class="flow">
            <h3 class="lead_flow"><img src="<?php bloginfo('template_url')?>/images/forstaff/lead_flow.png" alt="お仕事紹介までの流れ" width="656" height="34"></h3>
            <img class="consul_img" src="<?php bloginfo('template_url')?>/images/forstaff/flow_image.png" alt="flow_image" width="655" height="133">
            <ul class="consul">
                <li><img src="<?php bloginfo('template_url')?>/images/forstaff/lead_tel.png" alt="電話で相談" width="204" height="45"><img src="<?php bloginfo('template_url')?>/images/forstaff/tel.png" alt="tel" width="451" height="45"></li>
                <li><img src="<?php bloginfo('template_url')?>/images/forstaff/lead_mail.png" alt="メールで相談" width="204" height="45"><a href="mailto:info@staff-marche.co.jp"><img src="<?php bloginfo('template_url')?>/images/forstaff/mail.png" alt="mail" width="451" height="45"></a></li>
            </ul><!--consul-->
            <img class="attention_form" src="<?php bloginfo('template_url')?>/images/common/form/attention_form.png" alt="attention_form" width="556" height="24">
            <div class="form_box">
           <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
            <?php endwhile; ?>
            <?php else : ?>
            現在、記事はありません
            <?php endif; ?>

            </div>
        </div>
        
    </div><!--content-->
   

</div><!--content_body-->
<?php get_footer(); ?>