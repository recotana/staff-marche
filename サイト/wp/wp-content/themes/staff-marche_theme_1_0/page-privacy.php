<?php
/*
Template Name: 個人情報の取扱について
*/
?>

<?php get_header(); ?>
<body class="page_privacy">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>個人情報の取扱いについて</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/privacy/title.png" alt="title" width="310" height="26"></h3>
    <p><?php the_content(); ?></p>
    
    <div class="section">
    </div><!--section-->
    <h4><img src="<?php bloginfo('template_url')?>/images/privacy/lead1.png" alt="lead1" width="153" height="19"></h4>
<?php $rows = get_field('section');
if($rows): ?>

<?php foreach($rows as $row): ?>
<div class="section">
<?php
$attachment_id = $row['title_img'];
$size = "full"; // (thumbnail, medium, large, full or custom size)
 
$image = wp_get_attachment_image_src( $attachment_id, $size );
// url = $image[0];
// width = $image[1];
// height = $image[2];
?>

<h5><img src="<?php echo $image[0]; ?>" alt="<?php echo $row['title']?>"/></h5>
<?php echo $row['content']; ?>
</div><!--section-->
<?php endforeach; ?>
<?php endif; ?>

<?php endwhile; ?>
<?php else : ?>
現在、記事はありません
<?php endif; ?>
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>

