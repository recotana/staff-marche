<?php
/*
Template Name: 拠点
*/
?>

<?php get_header(); ?>
<body class="page_shops">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>拠点情報</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/shops/title.png" alt="title" width="672" height="92"></h3>
                <div class="content_box">
                    <h4><img src="<?php bloginfo('template_url')?>/images/shops/lead1.png" alt="本社" width="61" height="20" class="map" ></h4>
                    <div class="section dash_border clearfix">
                        <div class="info">
                            
                            <img src="<?php bloginfo('template_url')?>/images/shops/obihiro.jpg" alt="obihiro" width="250" height="166" style="margin-bottom:10px;"/>
                            <p>
〒080-0013<br/>
北海道帯広市西3条南9丁目2番地<br/>
太洋電気大同生命ビル5F<br/>
<br/>
TEL 0155-67-4509<br/>
FAX 0155-67-4609<br/>
E-mail <a href="mailto:info@staff-marche.co.jp">info@staff-marche.co.jp</a>
                            </p>
                        </div>
                        <img src="<?php bloginfo('template_url')?>/images/shops/map1.png" alt="map1" width="345" height="305">
                    </div><!--section-->
                    <h4><img src="<?php bloginfo('template_url')?>/images/shops/lead2.png" alt="札幌オフィス" width="118" height="20"></h4>
                    <div class="section clearfix">
                        <div class="info">
                            
                            <img src="<?php bloginfo('template_url')?>/images/shops/sapporo.jpg" alt="sapporo" width="250" height="167" style="margin-bottom:10px;"/>
                            <p>
〒060-0002<br/>
北海道札幌市中央区北2条西10丁目2番地7<br/>
Wall 2F<br/>
<br/>
TEL 011-205-0751<br/>
FAX 011-205-0752<br/>
E-mail <a href="mailto:info-sapporo@staff-marche.co.jp">info-sapporo@staff-marche.co.jp</a>
                            </p>
                        </div>
                        <img src="<?php bloginfo('template_url')?>/images/shops/map2.png" alt="map2" width="345" height="305" class="map" >
                    </div><!--section-->
                </div><!--content_box-->
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>

