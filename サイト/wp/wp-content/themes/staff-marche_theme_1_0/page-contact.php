<?php
/*
Template Name: お問い合わせ
*/
?>
<?php get_header(); ?>
<body class="page_contact">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>お問い合わせ</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
<h3><img src="<?php bloginfo('template_url')?>/images/contact/title.png" alt="title" width="228" height="27" /></h3>
<div class="form_box">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
<?php else : ?>
現在、記事はありません
<?php endif; ?>
</div>
</div><!--content-->


</div><!--content_body-->
<?php get_footer(); ?>