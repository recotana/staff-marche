<?php
/*
Template Name: 会社概要
*/
?>

<?php get_header(); ?>
<body class="page_aboutus">
<div id="wrapper">
<?php get_template_part('parts/head_nav') ?>
<ul id="breadcrumb" class="clearfix">
    <li><a href="<?php bloginfo('url')?>/">ホーム</a></li>
    <li>会社概要</li>
</ul><!--breadcrumb-->
<div id="content_body" class="clearfix">


<?php get_template_part('parts/side_nav') ?>

<div id="content">
    <h3 class="title"><img src="<?php bloginfo('template_url')?>/images/aboutus/title.png" alt="title" width="672" height="92"></h3>
    <div class="content_box">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

        <div class="section clearfix">
            <table>
                <tr>
                    <th><img src="<?php bloginfo('template_url')?>/images/aboutus/tlead1.png" alt="tlead1" width="96" height="14"></th>
                    <td><?php echo get_field('name'); ?></td>
                </tr>
                <tr>
                    <th><img src="<?php bloginfo('template_url')?>/images/aboutus/tlead2.png" alt="tlead1" width="96" height="14"></th>
                    <td><?php echo get_field('address'); ?></td>
                </tr>
                <tr>
                    <th><img src="<?php bloginfo('template_url')?>/images/aboutus/tlead3.png" alt="tlead1" width="96" height="14"></th>
                    <td><?php echo get_field('president'); ?></td>
                </tr>
                <tr>
                    <th><img src="<?php bloginfo('template_url')?>/images/aboutus/tlead4.png" alt="tlead1" width="96" height="14"></th>
                    <td><?php echo get_field('establish'); ?></td>
                </tr>
                <tr>
                    <th><img src="<?php bloginfo('template_url')?>/images/aboutus/tlead5.png" alt="tlead1" width="96" height="14"></th>
                    <td><?php echo get_field('capital'); ?></td>
                </tr>
                <tr>
                    <th><img src="<?php bloginfo('template_url')?>/images/aboutus/tlead6.png" alt="tlead1" width="96" height="14"></th>
                    <td><?php echo get_field('business'); ?></td>
                </tr>
                <tr>
                    <th><img src="<?php bloginfo('template_url')?>/images/aboutus/tlead7.png" alt="tlead1" width="96" height="14"></th>
                    <td><?php echo get_field('lisence'); ?></td>
                </tr>
            </table>
        </div><!--section-->
    </div><!--content_box-->
<?php endwhile; ?>
<?php else : ?>
現在、記事はありません
<?php endif; ?>
</div><!--content-->
</div><!--content_body-->
<?php get_footer(); ?>

